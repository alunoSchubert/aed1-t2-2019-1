#ifndef _FILA_H_
#define _FILA_H_

#include "arvore.h"

typedef struct elemf{
	struct nodo * node;
	struct elemf * next, * prev;
} elemf;

struct fila{
	elemf * header;
	int tam;
};

/** Cria uma fila
 * @return  um descritor ou NULL
 */
struct fila * fila_create();

/** Apaga todos elementos da fila
 * @param p descritor da fila
 * @return 1 se OK, 0 se erro
 */
int fila_makenull(struct fila * f);

/* Retorna o elemento mais antigo da fila ou zero se não existir
 * @param p descritor da fila
 * @return o elemento ou NULL
 */
struct nodo * dequeue(struct fila * f);

/* Insere um elemento no fim da fila
 * @param p descritor de fila
 * @param val elemento a ser inserido
 * @return 1 se OK, 0 se erro
 */
int enqueue(struct fila * f, struct nodo * n);

/* Retorna se a fila está vazia ou não
 * @param p descritor de fila
 * @return 1 se vazia, 0 se não
 */
 int fila_vazia(struct fila * f);
/** Desaloca toda a fila
  * @param p descritor da fila
  */
void fila_destroy(struct fila * f);

#endif