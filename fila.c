#include "fila.h"
#include <stdlib.h>

struct fila * fila_create()
{
    struct fila * q;

    if((q = (struct fila *)malloc(sizeof(struct fila))) != NULL)
    {
        q->tam = 0;
        if((q->header = (elemf *)malloc(sizeof(elemf))) != NULL)
        {
            q->header->next = q->header;
            q->header->prev = q->header;
            q->header->node = NULL;
            return q;
        }
        else
            return NULL;
    } else
        return NULL;
}

int fila_makenull(struct fila * f)
{
    if(f->tam == 0)
        return 0;
    else
    {
        while(dequeue(f));
        return 1;
    }
}

struct nodo * dequeue(struct fila * f)
{
    elemf * deq;
    struct nodo * node;

    if(f->tam == 0)
        return NULL;
    else if(f->tam == 1)
    {
        deq = f->header->next;
        node = deq->node;
        f->header->next = f->header;
        f->header->prev = f->header;
    } else {
        deq = f->header->next;
        node = deq->node;
        f->header->next = deq->next;
        deq->prev = f->header;
    }
    
    f->tam--;
    free(deq);
    return node;
}

int enqueue(struct fila * f, struct nodo * n)
{
    elemf * node, * temp;

    if((node = (elemf *)malloc(sizeof(elemf))) != NULL)
    {
        node->node = n;
        
        if(f->tam == 0)
        {
            f->header->next = node;
            f->header->prev = node;

            node->next = f->header;
            node->prev = f->header;
        } else {
            temp = f->header->prev;

            temp->next = node;
            node->prev = temp;

            f->header->prev = node;
            node->next = f->header;
        }
        f->tam++;
        return 1;
    } else
        return 0;
}

int fila_vazia(struct fila * f)
{
    if(f->tam == 0)
        return 1;
    else
        return 0;
}

void fila_destroy(struct fila * f)
{
    free(f);
}