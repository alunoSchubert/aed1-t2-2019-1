CC=gcc

CFLAGS=-Wall -Wextra -Werror -O0 -g -std=c11 -I.. 

.PHONY: all clean


all: clean grade

grade: test
	./test

test: arvore.o test.o fila.o
	$(CC) $(CFLAGS) arvore.o test.o fila.o -o test -lm

arvore.o: arvore.c
	$(CC) $(CFLAGS) -c arvore.c

# pilha.o: pilha.c
# 	$(CC) $(CFLAGS) -c pilha.c

fila.o: fila.c
	$(CC) $(CFLAGS) -c fila.c

test.o: test.c
	$(CC) $(CFLAGS) -c test.c -lm

clean:
	rm -rf *.o test test.dSYM