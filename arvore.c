#include <stdio.h>
#include <stdlib.h>
#include "arvore.h"
#include "fila.h"

/* Essa função remove as folhas da árvore */
struct nodo * rem(struct nodo * raiz);

struct nodo * inicializa_arvore(int entradas, int * valores)
{
    int i = 0;
    struct nodo * raiz;

    if(entradas <= 0 || valores == NULL)
        return NULL;

    if((raiz = (struct nodo *)malloc(sizeof(struct nodo))) != NULL) {
        raiz->valor = valores[i];
        raiz->esq = NULL;
        raiz->dir = NULL;
        for(i = 1; i < entradas; ++i) {
            if((insere_nodo(raiz, valores[i])) == NULL)
                return NULL;
        }
    } else {
        return NULL;
    }

    return raiz;
}

struct nodo * insere_nodo(struct nodo * raiz, int valor)
{
    struct nodo * temp;

    if(raiz == NULL) {
        if((temp = (struct nodo *)malloc(sizeof(struct nodo))) != NULL) {
            temp->valor = valor;
            temp->esq = NULL;
            temp->dir = NULL;
            return temp;
        } else
            return NULL;
    } else if(valor > raiz->valor) {
        raiz->dir = insere_nodo(raiz->dir, valor);
        return raiz;
    } else if(valor < raiz->valor) {
        raiz->esq = insere_nodo(raiz->esq, valor);
        return raiz;
    }
    return raiz;
}

struct nodo * remove_nodo(struct nodo * raiz, int valor)
{
    struct nodo * temp;
    
    if(raiz == NULL) return raiz;

    if(valor < raiz->valor)
        raiz->esq = remove_nodo(raiz->esq, valor);
    else if(valor > raiz->valor)
        raiz->dir = remove_nodo(raiz->dir, valor);
    else {
        if(raiz->esq == NULL) {
            temp = raiz->dir;
            free(raiz);
            return temp;
        } else if(raiz->dir == NULL) {
            temp = raiz->esq;
            free(raiz);
            return temp;
        }
        temp = raiz->dir;
        while(temp->esq != NULL && temp)
            temp = temp->esq;
        raiz->valor = temp->valor;
        raiz->dir = remove_nodo(raiz->dir, temp->valor);
    }
    return raiz;
}

int altura(struct nodo * raiz)
{
    int alt_esq, alt_dir;

    if(raiz == NULL)
        return 0;
    else {
        alt_esq = altura(raiz->esq);
        alt_dir = altura(raiz->dir);

        if(alt_esq > alt_dir)
            return alt_esq + 1;
        else
            return alt_dir + 1;
    }
}

struct nodo * busca(struct nodo * raiz, int valor)
{
    if(raiz == NULL)
        return NULL;
    else if(raiz->valor == valor)
        return raiz;
    else if(valor > raiz->valor)
        return busca(raiz->dir, valor);
    else
        return busca(raiz->esq, valor);
}

int balanceada(struct nodo * raiz)
{
    int alt_esq, alt_dir;

    if(raiz == NULL)
        return 1;

    alt_esq = altura(raiz->esq);
    alt_dir = altura(raiz->dir);

    if((alt_esq - alt_dir <= 1) && balanceada(raiz->esq) && balanceada(raiz->dir))
        return 1;

    return 0;
}

int numero_elementos(struct nodo * raiz)
{
    int hesq = 0, hdir = 0;

    if(raiz == NULL) {
        return 0;
        printf("%d", raiz->valor);
    } else {
        hesq = numero_elementos(raiz->esq);
        hdir = numero_elementos(raiz->dir);
        return hesq + hdir + 1;
    }
}


int abrangencia(struct nodo * raiz, int * resultado)
{
    struct fila * f = fila_create();
    struct nodo * aux;
    int i = 0;

    if(raiz != NULL) {
        enqueue(f, raiz);

        while(!fila_vazia(f)) {
            aux = dequeue(f);

            if(aux->esq != NULL)
                enqueue(f, aux->esq);
            if(aux->dir != NULL)
                enqueue(f, aux->dir);
            
            resultado[i++] = aux->valor;
        }

        fila_destroy(f);

        return i;    
    } else 
        return 0;
}

int tam;

int prefix(struct nodo * raiz, int * resultado)
{
    if(raiz == NULL)
        return 0;
    if(tam == 15)
        tam = 0;
    resultado[tam++] = raiz->valor;
    prefix(raiz->esq, resultado);
    prefix(raiz->dir, resultado);
    return tam;
}

int postfix(struct nodo * raiz, int * resultado)
{
    if(raiz == NULL)
        return 0;
    if(tam == 15)
        tam = 0;
    postfix(raiz->esq, resultado);
    postfix(raiz->dir, resultado);
    resultado[tam++] = raiz->valor;
    return tam;
}


int infix(struct nodo * raiz, int * resultado)
{
    if(raiz == NULL)
        return 0;
    if(tam == 15)
        tam = 0;
    infix(raiz->esq, resultado);
    *resultado = raiz->valor;
    resultado++;
    infix(raiz->dir, resultado);
    return tam;
}

void imprime(int * valores, int tamanho)
{
    for(int i = 0; i < tamanho; ++i) {
        if(((i % 9 == 0) && (i > 1)) || (i == tamanho-1))
            printf("%d\n", valores[i]);
        else
            printf("%d ", valores[i]);
    }
}

void remove_todos(struct nodo * raiz)
{
    while(raiz->esq != NULL || raiz->esq != NULL)
        rem(raiz);
    raiz = NULL;
}

struct nodo * rem(struct nodo * raiz)
{
    if(raiz == NULL)
        return NULL;

    if(raiz->esq == NULL && raiz->dir == NULL) {
        free(raiz);
        return NULL;
    }

    raiz->esq = rem(raiz->esq);
    raiz->dir = rem(raiz->dir);

    return raiz;
}